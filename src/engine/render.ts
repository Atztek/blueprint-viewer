
import {
  BoxGeometry,
  Mesh,
  MeshBasicMaterial,
    PerspectiveCamera,
    Scene,
    WebGLRenderer,
  } from "three";
  
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

export interface IRender{
  element: HTMLElement | null;
  clear(): void;
  atachElement(element: HTMLElement) : void;
}

export class Render2D implements IRender{
  element: HTMLElement | null = null;
  constructor(){}

  clear(){
    // empty
  }
  atachElement(element: HTMLElement){
    this.element = element;
  }
}

export class VoxelRender{
    scene: Scene;
    renderer: WebGLRenderer;
    camera: PerspectiveCamera;
    element: HTMLElement | null = null;

    constructor() {
      this.camera = new PerspectiveCamera(
        70,
        window.innerWidth / window.innerHeight,
        1,
        1000
      );
      this.camera.position.z = 400;
  
      this.scene = new Scene();
  
      this.renderer = new WebGLRenderer({ antialias: true });
      this.renderer.setPixelRatio(window.devicePixelRatio);
      this.renderer.setSize(window.innerWidth, window.innerHeight);
  
      const controls = new OrbitControls(this.camera, this.renderer.domElement);
  
      document.body.appendChild(this.renderer.domElement);
      
      const geometry = new BoxGeometry( 1, 1, 1 );
      const material = new MeshBasicMaterial( {color: 0x00ff00} );
      const cube = new Mesh( geometry, material );
      this.scene.add(cube);
      window.addEventListener("resize", ()=> { this.onWindowResize() }, false);
      this.camera.position.set(0, 20, 100);
      controls.update();
      this.renderer.render(this.scene, this.camera);
      this.animate();
    }
    
    atachElement(element: HTMLElement){
      this.element = element;
    }

    onWindowResize() {
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
  
      this.renderer.setSize(window.innerWidth, window.innerHeight);
    }
  
    animate() {
      requestAnimationFrame(this.animate.bind(this));
      this.renderer.render(this.scene, this.camera);
    }
    
    clear(){
      this.scene.clear()
    }
  }