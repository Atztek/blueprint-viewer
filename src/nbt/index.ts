

import { ListPayload, NBTParser, TagPayload, TagData } from "mc-anvil";


import {
    BoxGeometry,
    MeshStandardMaterial,
    Mesh,
    MeshBasicMaterial,
    TextureLoader,
    NearestFilter,
} from "three";

const types = {
    8: "STRING"
}

class Block2d {
    texture: string = "";

    half: string | null = null;

    type: string | null = null;

    setTexture(text: string) {
        this.texture = text;
    }

    /*
    setPropery(propertys: any) {
        this.propertys = propertys;
    }
    */

    getStyle() {
        const [mode, text] = this.texture.split(":");
        const style: { [id: string]: string } = {
            background: `url(/public/textures/minecraft/block/${text}.png)`
        }

        if (this.half && this.half == "top" || this.type && this.type == "top") {
            style.transform = "rotateX(180deg)";
        }

        return style;
    }
}

const parseToMap = (data: any) => {
    const ret = new Map();
    if (Array.isArray(data)) {
        for (const item of data) {
            if (item instanceof Object && 'type' in item) {
                ret.set(item.name, item.data);
            }
        }
    }
    return ret;
}

const parsePallet = (data: Array<TagData>) => {

    const ret = new Block2d();
    const map = parseToMap(data)

    if (map.has("Name")) {
        ret.setTexture(map.get("Name") as string);
    }

    if (map.has("Properties")) {
        const props = parseToMap(map.get("Properties"))
        ret.half = props.get("half")
        ret.type = props.get("type")
    }


    return ret;
}
export class NBT {
    parser: NBTParser;
    nodes: Map<string, TagPayload | TagPayload[]> = new Map();

    constructor(buffer: ArrayBuffer) {
        this.parser = new NBTParser(buffer);
        const data = this.parser.getTag();
        console.log(data);
        if (Array.isArray(data.data)) {
            for (const item of data.data) {
                if (item instanceof Object && 'type' in item) {
                    this.nodes.set(item.name, item.data);
                }
            }
        }
    }

    getTag(name: string): TagPayload {
        if (this.nodes.has(name)) {
            // @ts-ignore
            return this.nodes.get(name);
        }
        throw new Error(`tag '${name}' is not exist`)
    }

    getPallete() {
        const arr = (this.getTag("palette") as ListPayload).data
        const pallete = [];
        for (const params of arr) {
            // @ts-ignore
            pallete.push(parsePallet(params));
        }
        return pallete;
    }
}


const createCubeMatrix = (y: number, z: number, x: number): CubeMatrix => {
    return [...Array(y)].map(s => [...Array(z)].map(g => Array(x)))
}

const createCubeMatrixInt = (y: number, z: number, x: number): CubeMatrixInt => {
    return [...Array(y)].map(s => [...Array(z)].map(g => Array(x)))
}

export type CubeMatrix = Block[][][];
export type CubeMatrixInt = number[][][];

const imgStone = "test.png";

export class Block {
    x: number;
    y: number;
    z: number;
    type: number;

    constructor(x: number, y: number, z: number, type: number) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.type = type;
    }


    createMesh() {
        const geometry = new BoxGeometry(1, 1, 1);
        var textureLoader = new TextureLoader();
        const crateTexture = textureLoader.load(imgStone);
        crateTexture.magFilter = NearestFilter;
        crateTexture.minFilter = NearestFilter;
        const material = new MeshBasicMaterial({
            color: 0xffffff,
            map: crateTexture,
        })

        const mesh = new Mesh(geometry, material);
        mesh.position.set(this.x, this.z, this.y);
        return mesh
    }
}
export class Blueprint {

    blockmatrix: CubeMatrixInt = [];
    blocks: Array<Block> = [];
    pallete: Array<Block2d> = [];
    constructor() {

    }

    setPallete(pallete: Array<Block2d>) {
        this.pallete = pallete;
    }

    init(data: Array<number>, size_y: number, size_z: number, size_x: number) {
        this.blockmatrix = createCubeMatrixInt(size_y, size_z, size_x);
        const totalLen = data.length * 2;
        const oneDimArray = Array(totalLen);

        data.forEach((value, index) => {
            oneDimArray[index * 2] = value >> 16;
            oneDimArray[(index * 2) + 1] = (value << 16) >> 16;
        })

        let i = 0;
        for (let y = 0; y < size_y; y++) {
            for (let z = 0; z < size_z; z++) {
                for (let x = 0; x < size_x; x++) {
                    this.blockmatrix[y][z][x] = oneDimArray[i++]
                }
            }
        }
    }

    getYLayer(y: number) {
        return this.blockmatrix[y];
    }

    getZLayer(z: number) {
        const ret = []
        for (let y = 0; y < this.blockmatrix.length; y++) {
            ret.push(this.blockmatrix[y][z]);
        }
        return ret.reverse();
    }

    getXLayer(x: number) {
        const ret = []
        for (let y = 0; y < this.blockmatrix.length; y++) {
            const temp = [];
            for (let z = 0; z < this.blockmatrix[y].length; z++) {
                temp.push(this.blockmatrix[y][z][x])
            }
            ret.push(temp);
        }
        return ret.reverse();
    }

    getSizes() {
        const y = this.blockmatrix.length;
        const z = this.blockmatrix[0] ? this.blockmatrix[0].length : 0;
        const x = (this.blockmatrix[0] && this.blockmatrix[0][0]) ? this.blockmatrix[0][0].length : 0;
        return { y, z, x }
    }

    *getYLayerFlat(y: number) {
        for (const z of this.blockmatrix[y]) {
            for (const x of z) {
                yield x;
            }
        }

    }
}
